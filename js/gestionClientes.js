function getClientes(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function(){
    //Se pone 4, el cual, significa que ya culminó la solicitud.
    //Se ingresa el 200 para esperar confirmación de que la página ha sido obtenida de forma satisfactoria.
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  //GET es para obtener datos.
  //PUT es para escribir.
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  //Para que te pinte la tabla en blanco y gris, tipo cebra
  tabla.classList.add("table-striped");
  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONClientes.value.length; i++){
      //console.log(JSONProductos.value[i].ProductName);
      //Primera fila
      var nuevaFila = document.createElement("tr");
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClientes.value[i].ContactName;
      //Segunda fila
      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText = JSONClientes.value[i].City;
      //Tercera fila
      var columnaBandera = document.createElement("td");
      var imgBandera = document.createElement("img");
      imgBandera.classList.add("flag");
      if(JSONClientes.value[i].Country == "UK") {
        imgBandera.src = rutaBandera + "United-Kingdom.png";
      }else {
        imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
      }
      columnaBandera.appendChild(imgBandera);

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaBandera);

      tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
