var productosObtenidos;

function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function(){
    //Se pone 4, el cual, significa que ya culminó la solicitud.
    //Se ingresa el 200 para esperar confirmación de que la página ha sido obtenida de forma satisfactoria.
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  //GET es para obtener datos.
  //PUT es para escribir.
  request.open("GET", url, true);
  request.send();
}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  //Para que te pinte la tabla en blanco y gris, tipo cebra
  tabla.classList.add("table-striped");
  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONProductos.value.length; i++){
      //console.log(JSONProductos.value[i].ProductName);
      //Primera fila
      var nuevaFila = document.createElement("tr");
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONProductos.value[i].ProductName;
      //Segunda fila
      var columnaPrecio = document.createElement("td");
      columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
      //Tercera fila
      var columnaStock = document.createElement("td");
      columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaPrecio);
      nuevaFila.appendChild(columnaStock);

      tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
