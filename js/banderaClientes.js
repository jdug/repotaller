var banderaxCliente;

function getBandera(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function(){
    //Se pone 4, el cual, significa que ya culminó la solicitud.
    //Se ingresa el 200 para esperar confirmación de que la página ha sido obtenida de forma satisfactoria.
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      banderaxCliente = request.responseText;
      procesarBanderas();
    }
  }
  //GET es para obtener datos.
  //PUT es para escribir.
  request.open("GET", url, true);
  request.send();
}

function procesarBanderas(){
  var JSONProductos = JSON.parse(banderaxCliente);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  //Para que te pinte la tabla en blanco y gris, tipo cebra
  tabla.classList.add("table-striped");
  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONProductos.value.length; i++){
      //console.log(JSONProductos.value[i].ProductName);
      var nuevaFila = document.createElement("tr");
      //Primera fila
      var columnaCliente = document.createElement("td");
      columnaCliente.innerText = JSONProductos.value[i].ProductName;
      //Segunda fila
      var columnaBandera = document.createElement("td");
      columnaBandera.innerText = ("https://www.countries-ofthe-world.com/flags-normal/flag-of-" +  + ".png"));

      nuevaFila.appendChild(columnaCliente);
      nuevaFila.appendChild(columnaBandera);

      tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
